# Changelog

## v0.2 

- Loading from and saving to disk for materials
- Added a script for scraping material data from the particle data group
[web page](https://pdg.lbl.gov/2020/AtomicNuclearProperties/)

### Breaking changes

#### Hard coded materials were deleted in favor of data files

i.e. previously used `AluminiumMaterial` must be loaded from disk, using
```python
from AtomicNuclearProperties import LoadMaterial
from pint import UnitRegistry
ureg = UnitRegistry()
Al = LoadMaterial("data/Al.txt", ureg)
```
These data files were scraped from the pdg page once, and don't need to be
regularly updated. Hence they were added to the repository.

#### camelCase in Material class members was changed to underscore-case

> e.g. `meanExcitationEnergy` → `mean_excitation_energy`

This allowed a simplification during config storing and writing

## v0.1

### Initial version of the repository

- Describe (simple and composite) materials
- Calculate the energy loss of according to the Bethe formula (no corrections)
- Model the energy loss of a particle as it steps through a material
