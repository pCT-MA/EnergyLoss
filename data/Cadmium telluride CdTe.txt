[Material]
friendly_name                   = Cadmium telluride CdTe
short_name                      = Cadmium telluride CdTe
specific_gravity                = 6.2 gram / centimeter ** 3
mean_excitation_energy          = 539.3 electron_volt
minimum_ionization              = 1.243 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 98.6 gram / centimeter ** 2
nuclear_interaction_length      = 167.3 gram / centimeter ** 2
pion_collision_length           = 122.8 gram / centimeter ** 2
pion_interaction_length         = 194.9 gram / centimeter ** 2
radiation_length                = 8.91 gram / centimeter ** 2
critical_energy_electrons       = 11.84 megaelectron_volt
critical_energy_positrons       = 11.44 megaelectron_volt
moliere_radius                  = 15.95 gram / centimeter ** 2
plasma_energy                   = 46.31 electron_volt
muon_critical_energy            = 208.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.41665

[Composition_0]
element                         = Cd
atomic_number                   = 48.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.468355

[Composition_1]
element                         = Te
atomic_number                   = 52.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.531645
