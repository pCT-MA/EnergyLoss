[Material]
friendly_name                   = Polonium
short_name                      = Po
atomic_number                   = 84
atomic_mass                     = 208.98243 gram / mole
specific_gravity                = 9.32 gram / centimeter ** 3
mean_excitation_energy          = 830.0 electron_volt
minimum_ionization              = 1.141 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 114.3 gram / centimeter ** 2
nuclear_interaction_length      = 200.2 gram / centimeter ** 2
pion_collision_length           = 137.6 gram / centimeter ** 2
pion_interaction_length         = 226.7 gram / centimeter ** 2
radiation_length                = 6.16 gram / centimeter ** 2
critical_energy_electrons       = 7.33 megaelectron_volt
critical_energy_positrons       = 7.06 megaelectron_volt
moliere_radius                  = 17.83 gram / centimeter ** 2
plasma_energy                   = 55.77 electron_volt
muon_critical_energy            = 140.0 gigaelectron_volt
