[Material]
friendly_name                   = Yttrium
short_name                      = Y
atomic_number                   = 39
atomic_mass                     = 88.90584 gram / mole
specific_gravity                = 4.469 gram / centimeter ** 3
mean_excitation_energy          = 379.0 electron_volt
minimum_ionization              = 1.359 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 91.3 gram / centimeter ** 2
nuclear_interaction_length      = 152.2 gram / centimeter ** 2
pion_collision_length           = 116.0 gram / centimeter ** 2
pion_interaction_length         = 180.3 gram / centimeter ** 2
radiation_length                = 10.41 gram / centimeter ** 2
critical_energy_electrons       = 15.3 megaelectron_volt
critical_energy_positrons       = 14.81 megaelectron_volt
moliere_radius                  = 14.43 gram / centimeter ** 2
plasma_energy                   = 40.35 electron_volt
muon_critical_energy            = 256.0 gigaelectron_volt
