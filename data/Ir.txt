[Material]
friendly_name                   = Iridium
short_name                      = Ir
atomic_number                   = 77
atomic_mass                     = 192.217 gram / mole
specific_gravity                = 22.42 gram / centimeter ** 3
mean_excitation_energy          = 757.0 electron_volt
minimum_ionization              = 1.134 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 111.7 gram / centimeter ** 2
nuclear_interaction_length      = 194.8 gram / centimeter ** 2
pion_collision_length           = 135.1 gram / centimeter ** 2
pion_interaction_length         = 221.4 gram / centimeter ** 2
radiation_length                = 6.59 gram / centimeter ** 2
critical_energy_electrons       = 7.67 megaelectron_volt
critical_energy_positrons       = 7.39 megaelectron_volt
moliere_radius                  = 18.24 gram / centimeter ** 2
plasma_energy                   = 86.36 electron_volt
muon_critical_energy            = 145.0 gigaelectron_volt
