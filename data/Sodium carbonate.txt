[Material]
friendly_name                   = Sodium carbonate
short_name                      = Sodium carbonate
specific_gravity                = 2.532 gram / centimeter ** 3
mean_excitation_energy          = 125.0 electron_volt
minimum_ionization              = 1.681 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 63.6 gram / centimeter ** 2
nuclear_interaction_length      = 94.6 gram / centimeter ** 2
pion_collision_length           = 90.3 gram / centimeter ** 2
pion_interaction_length         = 125.7 gram / centimeter ** 2
radiation_length                = 31.72 gram / centimeter ** 2
critical_energy_electrons       = 58.48 megaelectron_volt
critical_energy_positrons       = 56.85 megaelectron_volt
moliere_radius                  = 11.5 gram / centimeter ** 2
plasma_energy                   = 32.12 electron_volt
muon_critical_energy            = 800.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.49062

[Composition_0]
element                         = C
atomic_number                   = 6.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.113323

[Composition_1]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 3.0
mass_fraction                   = 0.452861

[Composition_2]
element                         = Na
atomic_number                   = 11.0
atomic_fraction                 = 2.0
mass_fraction                   = 0.433815
