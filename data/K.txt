[Material]
friendly_name                   = Potassium
short_name                      = K
atomic_number                   = 19
atomic_mass                     = 39.0983 gram / mole
specific_gravity                = 0.862 gram / centimeter ** 3
mean_excitation_energy          = 190.0 electron_volt
minimum_ionization              = 1.623 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 75.4 gram / centimeter ** 2
nuclear_interaction_length      = 119.0 gram / centimeter ** 2
pion_collision_length           = 101.0 gram / centimeter ** 2
pion_interaction_length         = 148.1 gram / centimeter ** 2
radiation_length                = 17.32 gram / centimeter ** 2
critical_energy_electrons       = 31.62 megaelectron_volt
critical_energy_positrons       = 30.72 megaelectron_volt
moliere_radius                  = 11.61 gram / centimeter ** 2
plasma_energy                   = 18.65 electron_volt
muon_critical_energy            = 472.0 gigaelectron_volt
