[Material]
friendly_name                   = Cesium fluoride CsF
short_name                      = Cesium fluoride CsF
specific_gravity                = 4.115 gram / centimeter ** 3
mean_excitation_energy          = 440.7 electron_volt
minimum_ionization              = 1.286 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 94.6 gram / centimeter ** 2
nuclear_interaction_length      = 157.5 gram / centimeter ** 2
pion_collision_length           = 119.6 gram / centimeter ** 2
pion_interaction_length         = 186.8 gram / centimeter ** 2
radiation_length                = 9.16 gram / centimeter ** 2
critical_energy_electrons       = 12.63 megaelectron_volt
critical_energy_positrons       = 12.21 megaelectron_volt
moliere_radius                  = 15.39 gram / centimeter ** 2
plasma_energy                   = 37.94 electron_volt
muon_critical_energy            = 217.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.42132

[Composition_0]
element                         = F
atomic_number                   = 9.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.125069

[Composition_1]
element                         = Cs
atomic_number                   = 55.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.874931
