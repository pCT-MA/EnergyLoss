[Material]
friendly_name                   = Barium sulfate
short_name                      = Barium sulfate
specific_gravity                = 4.5 gram / centimeter ** 3
mean_excitation_energy          = 285.7 electron_volt
minimum_ionization              = 1.406 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 82.4 gram / centimeter ** 2
nuclear_interaction_length      = 131.0 gram / centimeter ** 2
pion_collision_length           = 109.1 gram / centimeter ** 2
pion_interaction_length         = 163.1 gram / centimeter ** 2
radiation_length                = 11.64 gram / centimeter ** 2
critical_energy_electrons       = 17.55 megaelectron_volt
critical_energy_positrons       = 17.0 megaelectron_volt
moliere_radius                  = 14.06 gram / centimeter ** 2
plasma_energy                   = 40.81 electron_volt
muon_critical_energy            = 285.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.44561

[Composition_0]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 4.0
mass_fraction                   = 0.274212

[Composition_1]
element                         = S
atomic_number                   = 16.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.137368

[Composition_2]
element                         = Ba
atomic_number                   = 56.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.58842
