[Material]
friendly_name                   = Tungsten hexafluoride
short_name                      = Tungsten hexafluoride
specific_gravity                = 2.4 gram / centimeter ** 3
mean_excitation_energy          = 354.4 electron_volt
minimum_ionization              = 1.346 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 87.1 gram / centimeter ** 2
nuclear_interaction_length      = 140.0 gram / centimeter ** 2
pion_collision_length           = 113.4 gram / centimeter ** 2
pion_interaction_length         = 171.5 gram / centimeter ** 2
radiation_length                = 9.72 gram / centimeter ** 2
critical_energy_electrons       = 14.03 megaelectron_volt
critical_energy_positrons       = 13.59 megaelectron_volt
moliere_radius                  = 14.69 gram / centimeter ** 2
plasma_energy                   = 29.27 electron_volt
muon_critical_energy            = 234.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.42976

[Composition_0]
element                         = F
atomic_number                   = 9.0
atomic_fraction                 = 6.0
mass_fraction                   = 0.382723

[Composition_1]
element                         = W
atomic_number                   = 74.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.617277
