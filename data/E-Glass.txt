[Material]
friendly_name                   = E-Glass
short_name                      = E-Glass
specific_gravity                = 2.61 gram / centimeter ** 3
mean_excitation_energy          = 143.4 electron_volt
minimum_ionization              = 1.673 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 65.7 gram / centimeter ** 2
nuclear_interaction_length      = 98.8 gram / centimeter ** 2
pion_collision_length           = 92.4 gram / centimeter ** 2
pion_interaction_length         = 129.9 gram / centimeter ** 2
radiation_length                = 25.88 gram / centimeter ** 2
critical_energy_electrons       = 47.59 megaelectron_volt
critical_energy_positrons       = 46.24 megaelectron_volt
moliere_radius                  = 11.53 gram / centimeter ** 2
plasma_energy                   = 32.82 electron_volt
muon_critical_energy            = 674.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.49689

[Composition_0]
element                         = B
atomic_number                   = 5.0
atomic_fraction                 = 0.09
mass_fraction                   = 0.031058

[Composition_1]
element                         = Mg
atomic_number                   = 12.0
atomic_fraction                 = 0.02
mass_fraction                   = 0.018094

[Composition_2]
element                         = Al
atomic_number                   = 13.0
atomic_fraction                 = 0.09
mass_fraction                   = 0.074093

[Composition_3]
element                         = Si
atomic_number                   = 14.0
atomic_fraction                 = 0.29
mass_fraction                   = 0.252411

[Composition_4]
element                         = Ca
atomic_number                   = 20.0
atomic_fraction                 = 0.11
mass_fraction                   = 0.135793

[Composition_5]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.488551
