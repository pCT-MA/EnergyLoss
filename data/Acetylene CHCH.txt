[Material]
friendly_name                   = Acetylene CHCH
short_name                      = Acetylene CHCH
specific_gravity                = 0.001097 gram / centimeter ** 3
mean_excitation_energy          = 58.2 electron_volt
minimum_ionization              = 2.025 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 57.5 gram / centimeter ** 2
nuclear_interaction_length      = 81.7 gram / centimeter ** 2
pion_collision_length           = 85.0 gram / centimeter ** 2
pion_interaction_length         = 113.7 gram / centimeter ** 2
radiation_length                = 43.79 gram / centimeter ** 2
critical_energy_electrons       = 116.34 megaelectron_volt
critical_energy_positrons       = 113.85 megaelectron_volt
moliere_radius                  = 7.98 gram / centimeter ** 2
plasma_energy                   = 0.7 electron_volt
muon_critical_energy            = 1400.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.53768

[Composition_0]
element                         = H
atomic_number                   = 1.0
atomic_fraction                 = 2.0
mass_fraction                   = 0.077418

[Composition_1]
element                         = C
atomic_number                   = 6.0
atomic_fraction                 = 2.0
mass_fraction                   = 0.922582
