[Material]
friendly_name                   = Lung ICRP
short_name                      = Lung ICRP
specific_gravity                = 1.05 gram / centimeter ** 3
mean_excitation_energy          = 75.3 electron_volt
minimum_ionization              = 1.97 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 58.6 gram / centimeter ** 2
nuclear_interaction_length      = 83.7 gram / centimeter ** 2
pion_collision_length           = 86.1 gram / centimeter ** 2
pion_interaction_length         = 115.6 gram / centimeter ** 2
radiation_length                = 36.49 gram / centimeter ** 2
critical_energy_electrons       = 78.72 megaelectron_volt
critical_energy_positrons       = 76.63 megaelectron_volt
moliere_radius                  = 9.83 gram / centimeter ** 2
plasma_energy                   = 21.89 electron_volt
muon_critical_energy            = 1031.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.54965

[Composition_0]
element                         = H
atomic_number                   = 1.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.101278

[Composition_1]
element                         = C
atomic_number                   = 6.0
atomic_fraction                 = 0.08
mass_fraction                   = 0.10231

[Composition_2]
element                         = N
atomic_number                   = 7.0
atomic_fraction                 = 0.02
mass_fraction                   = 0.02865

[Composition_3]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 0.47
mass_fraction                   = 0.757072

[Composition_4]
element                         = Na
atomic_number                   = 11.0
atomic_fraction                 = 0.0
mass_fraction                   = 0.00184

[Composition_5]
element                         = Mg
atomic_number                   = 12.0
atomic_fraction                 = 0.0
mass_fraction                   = 0.00073

[Composition_6]
element                         = P
atomic_number                   = 15.0
atomic_fraction                 = 0.0
mass_fraction                   = 0.0008

[Composition_7]
element                         = S
atomic_number                   = 16.0
atomic_fraction                 = 0.0
mass_fraction                   = 0.00225

[Composition_8]
element                         = Cl
atomic_number                   = 17.0
atomic_fraction                 = 0.0
mass_fraction                   = 0.00266

[Composition_9]
element                         = K
atomic_number                   = 19.0
atomic_fraction                 = 0.0
mass_fraction                   = 0.00194

[Composition_10]
element                         = Ca
atomic_number                   = 20.0
atomic_fraction                 = 0.0
mass_fraction                   = 9e-05

[Composition_11]
element                         = Fe
atomic_number                   = 26.0
atomic_fraction                 = 0.0
mass_fraction                   = 0.00037

[Composition_12]
element                         = Zn
atomic_number                   = 30.0
atomic_fraction                 = 0.0
mass_fraction                   = 1e-05
