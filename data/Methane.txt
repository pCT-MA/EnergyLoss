[Material]
friendly_name                   = Methane
short_name                      = Methane
specific_gravity                = 0.0006672 gram / centimeter ** 3
mean_excitation_energy          = 41.7 electron_volt
minimum_ionization              = 2.417 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 54.0 gram / centimeter ** 2
nuclear_interaction_length      = 73.8 gram / centimeter ** 2
pion_collision_length           = 81.8 gram / centimeter ** 2
pion_interaction_length         = 105.4 gram / centimeter ** 2
radiation_length                = 46.47 gram / centimeter ** 2
critical_energy_electrons       = 146.86 megaelectron_volt
critical_energy_positrons       = 143.82 megaelectron_volt
moliere_radius                  = 6.71 gram / centimeter ** 2
plasma_energy                   = 0.59 electron_volt
muon_critical_energy            = 1716.0 gigaelectron_volt
melting_point                   = 90.68 kelvin
boiling_point                   = 111.7 kelvin
atomic_number_mass_number_ratio = 0.62334

[Composition_0]
element                         = H
atomic_number                   = 1.0
atomic_fraction                 = 4.0
mass_fraction                   = 0.251306

[Composition_1]
element                         = C
atomic_number                   = 6.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.748694
