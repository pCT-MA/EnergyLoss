[Material]
friendly_name                   = Calcium oxide CaO
short_name                      = Calcium oxide CaO
specific_gravity                = 3.3 gram / centimeter ** 3
mean_excitation_energy          = 176.1 electron_volt
minimum_ionization              = 1.65 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 71.0 gram / centimeter ** 2
nuclear_interaction_length      = 109.5 gram / centimeter ** 2
pion_collision_length           = 97.4 gram / centimeter ** 2
pion_interaction_length         = 140.0 gram / centimeter ** 2
radiation_length                = 19.01 gram / centimeter ** 2
critical_energy_electrons       = 34.21 megaelectron_volt
critical_energy_positrons       = 33.21 megaelectron_volt
moliere_radius                  = 11.78 gram / centimeter ** 2
plasma_energy                   = 36.99 electron_volt
muon_critical_energy            = 507.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.49929

[Composition_0]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.285299

[Composition_1]
element                         = Ca
atomic_number                   = 20.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.714701
