[Material]
friendly_name                   = Trichloroethylene
short_name                      = Trichloroethylene
specific_gravity                = 1.46 gram / centimeter ** 3
mean_excitation_energy          = 148.1 electron_volt
minimum_ionization              = 1.656 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 70.2 gram / centimeter ** 2
nuclear_interaction_length      = 107.8 gram / centimeter ** 2
pion_collision_length           = 96.5 gram / centimeter ** 2
pion_interaction_length         = 138.3 gram / centimeter ** 2
radiation_length                = 21.55 gram / centimeter ** 2
critical_energy_electrons       = 39.45 megaelectron_volt
critical_energy_positrons       = 38.35 megaelectron_volt
moliere_radius                  = 11.58 gram / centimeter ** 2
plasma_energy                   = 24.3 electron_volt
muon_critical_energy            = 569.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.4871

[Composition_0]
element                         = H
atomic_number                   = 1.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.007671

[Composition_1]
element                         = C
atomic_number                   = 6.0
atomic_fraction                 = 2.0
mass_fraction                   = 0.182831

[Composition_2]
element                         = Cl
atomic_number                   = 17.0
atomic_fraction                 = 3.0
mass_fraction                   = 0.809498
