[Material]
friendly_name                   = Ethylene
short_name                      = Ethylene
specific_gravity                = 0.001175 gram / centimeter ** 3
mean_excitation_energy          = 50.7 electron_volt
minimum_ionization              = 2.174 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 56.1 gram / centimeter ** 2
nuclear_interaction_length      = 78.5 gram / centimeter ** 2
pion_collision_length           = 83.7 gram / centimeter ** 2
pion_interaction_length         = 110.4 gram / centimeter ** 2
radiation_length                = 44.77 gram / centimeter ** 2
critical_energy_electrons       = 126.75 megaelectron_volt
critical_energy_positrons       = 124.06 megaelectron_volt
moliere_radius                  = 7.49 gram / centimeter ** 2
plasma_energy                   = 0.75 electron_volt
muon_critical_energy            = 1508.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.57034

[Composition_0]
element                         = H
atomic_number                   = 1.0
atomic_fraction                 = 4.0
mass_fraction                   = 0.143711

[Composition_1]
element                         = C
atomic_number                   = 6.0
atomic_fraction                 = 2.0
mass_fraction                   = 0.856289
