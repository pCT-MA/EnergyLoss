[Material]
friendly_name                   = Silver chloride AgCl
short_name                      = Silver chloride AgCl
specific_gravity                = 5.56 gram / centimeter ** 3
mean_excitation_energy          = 398.4 electron_volt
minimum_ionization              = 1.367 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 89.2 gram / centimeter ** 2
nuclear_interaction_length      = 147.2 gram / centimeter ** 2
pion_collision_length           = 114.4 gram / centimeter ** 2
pion_interaction_length         = 176.0 gram / centimeter ** 2
radiation_length                = 10.34 gram / centimeter ** 2
critical_energy_electrons       = 15.22 megaelectron_volt
critical_energy_positrons       = 14.72 megaelectron_volt
moliere_radius                  = 14.41 gram / centimeter ** 2
plasma_energy                   = 45.41 electron_volt
muon_critical_energy            = 256.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.44655

[Composition_0]
element                         = Cl
atomic_number                   = 17.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.247368

[Composition_1]
element                         = Ag
atomic_number                   = 47.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.752632
