[Material]
friendly_name                   = Caesium
short_name                      = Cs
atomic_number                   = 55
atomic_mass                     = 132.90545196 gram / mole
specific_gravity                = 1.873 gram / centimeter ** 3
mean_excitation_energy          = 488.0 electron_volt
minimum_ionization              = 1.254 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 101.2 gram / centimeter ** 2
nuclear_interaction_length      = 172.8 gram / centimeter ** 2
pion_collision_length           = 125.3 gram / centimeter ** 2
pion_interaction_length         = 200.2 gram / centimeter ** 2
radiation_length                = 8.31 gram / centimeter ** 2
critical_energy_electrons       = 11.34 megaelectron_volt
critical_energy_positrons       = 10.97 megaelectron_volt
moliere_radius                  = 15.53 gram / centimeter ** 2
plasma_energy                   = 25.37 electron_volt
muon_critical_energy            = 200.0 gigaelectron_volt
