[Material]
friendly_name                   = Deuterium oxide liquid
short_name                      = Deuterium oxide liquid
specific_gravity                = 1.107 gram / centimeter ** 3
mean_excitation_energy          = 79.7 electron_volt
minimum_ionization              = 1.782 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 59.0 gram / centimeter ** 2
nuclear_interaction_length      = 85.7 gram / centimeter ** 2
pion_collision_length           = 87.0 gram / centimeter ** 2
pion_interaction_length         = 119.3 gram / centimeter ** 2
radiation_length                = 40.11 gram / centimeter ** 2
critical_energy_electrons       = 78.33 megaelectron_volt
critical_energy_positrons       = 76.24 megaelectron_volt
moliere_radius                  = 10.86 gram / centimeter ** 2
plasma_energy                   = 21.42 electron_volt
muon_critical_energy            = 967.0 gigaelectron_volt
melting_point                   = 277.0 kelvin
boiling_point                   = 374.5 kelvin
index_of_refraction             = 1.328
atomic_number_mass_number_ratio = 0.49931

[Composition_0]
element                         = D
atomic_number                   = 1.0
atomic_fraction                 = 2.0
mass_fraction                   = 0.201133

[Composition_1]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.798867
