[Material]
friendly_name                   = Bromine gas
short_name                      = Bromine gas
atomic_number                   = 35
atomic_mass                     = 79.904 gram / mole
specific_gravity                = 0.007072 gram / centimeter ** 3
mean_excitation_energy          = 343.0 electron_volt
minimum_ionization              = 1.388 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 88.9 gram / centimeter ** 2
nuclear_interaction_length      = 147.2 gram / centimeter ** 2
pion_collision_length           = 113.8 gram / centimeter ** 2
pion_interaction_length         = 175.5 gram / centimeter ** 2
radiation_length                = 11.42 gram / centimeter ** 2
critical_energy_electrons       = 19.16 megaelectron_volt
critical_energy_positrons       = 18.6 megaelectron_volt
moliere_radius                  = 12.64 gram / centimeter ** 2
plasma_energy                   = 1.6 electron_volt
muon_critical_energy            = 327.0 gigaelectron_volt
melting_point                   = 265.9 kelvin
boiling_point                   = 331.9 kelvin
