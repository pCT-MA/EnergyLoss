[Material]
friendly_name                   = Nitrous oxide
short_name                      = Nitrous oxide
specific_gravity                = 0.001831 gram / centimeter ** 3
mean_excitation_energy          = 84.9 electron_volt
minimum_ionization              = 1.819 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 61.2 gram / centimeter ** 2
nuclear_interaction_length      = 89.9 gram / centimeter ** 2
pion_collision_length           = 88.4 gram / centimeter ** 2
pion_interaction_length         = 121.8 gram / centimeter ** 2
radiation_length                = 36.53 gram / centimeter ** 2
critical_energy_electrons       = 87.1 megaelectron_volt
critical_energy_positrons       = 85.16 megaelectron_volt
moliere_radius                  = 8.89 gram / centimeter ** 2
plasma_energy                   = 0.87 electron_volt
muon_critical_energy            = 1104.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.49985

[Composition_0]
element                         = N
atomic_number                   = 7.0
atomic_fraction                 = 2.0
mass_fraction                   = 0.636483

[Composition_1]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.363517
