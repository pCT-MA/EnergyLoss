[Material]
friendly_name                   = Pyridine
short_name                      = Pyridine
specific_gravity                = 0.9819 gram / centimeter ** 3
mean_excitation_energy          = 66.2 electron_volt
minimum_ionization              = 1.918 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 58.1 gram / centimeter ** 2
nuclear_interaction_length      = 83.0 gram / centimeter ** 2
pion_collision_length           = 85.5 gram / centimeter ** 2
pion_interaction_length         = 115.0 gram / centimeter ** 2
radiation_length                = 42.64 gram / centimeter ** 2
critical_energy_electrons       = 89.7 megaelectron_volt
critical_energy_positrons       = 87.34 megaelectron_volt
moliere_radius                  = 10.08 gram / centimeter ** 2
plasma_energy                   = 20.81 electron_volt
muon_critical_energy            = 1145.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.53096

[Composition_0]
element                         = H
atomic_number                   = 1.0
atomic_fraction                 = 5.0
mass_fraction                   = 0.06371

[Composition_1]
element                         = C
atomic_number                   = 6.0
atomic_fraction                 = 5.0
mass_fraction                   = 0.759217

[Composition_2]
element                         = N
atomic_number                   = 7.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.177073
