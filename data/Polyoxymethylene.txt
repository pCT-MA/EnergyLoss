[Material]
friendly_name                   = Polyoxymethylene
short_name                      = Polyoxymethylene
specific_gravity                = 1.425 gram / centimeter ** 3
mean_excitation_energy          = 77.4 electron_volt
minimum_ionization              = 1.894 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 58.8 gram / centimeter ** 2
nuclear_interaction_length      = 84.3 gram / centimeter ** 2
pion_collision_length           = 86.2 gram / centimeter ** 2
pion_interaction_length         = 116.2 gram / centimeter ** 2
radiation_length                = 38.47 gram / centimeter ** 2
critical_energy_electrons       = 79.63 megaelectron_volt
critical_energy_positrons       = 77.49 megaelectron_volt
moliere_radius                  = 10.24 gram / centimeter ** 2
plasma_energy                   = 25.11 electron_volt
muon_critical_energy            = 1040.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.53287

[Composition_0]
element                         = H
atomic_number                   = 1.0
atomic_fraction                 = 2.0
mass_fraction                   = 0.067135

[Composition_1]
element                         = C
atomic_number                   = 6.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.400017

[Composition_2]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.532848
