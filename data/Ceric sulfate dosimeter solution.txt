[Material]
friendly_name                   = Ceric sulfate dosimeter solution
short_name                      = Ceric sulfate dosimeter solution
specific_gravity                = 1.03 gram / centimeter ** 3
mean_excitation_energy          = 76.7 electron_volt
minimum_ionization              = 1.979 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 58.8 gram / centimeter ** 2
nuclear_interaction_length      = 83.9 gram / centimeter ** 2
pion_collision_length           = 86.3 gram / centimeter ** 2
pion_interaction_length         = 115.8 gram / centimeter ** 2
radiation_length                = 35.35 gram / centimeter ** 2
critical_energy_electrons       = 76.64 megaelectron_volt
critical_energy_positrons       = 74.6 megaelectron_volt
moliere_radius                  = 9.78 gram / centimeter ** 2
plasma_energy                   = 21.74 electron_volt
muon_critical_energy            = 1009.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.55279

[Composition_0]
element                         = H
atomic_number                   = 1.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.107596

[Composition_1]
element                         = N
atomic_number                   = 7.0
atomic_fraction                 = 0.0
mass_fraction                   = 0.0008

[Composition_2]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 0.51
mass_fraction                   = 0.874976

[Composition_3]
element                         = S
atomic_number                   = 16.0
atomic_fraction                 = 0.0
mass_fraction                   = 0.014627

[Composition_4]
element                         = Ce
atomic_number                   = 58.0
atomic_fraction                 = 0.0
mass_fraction                   = 0.002001
