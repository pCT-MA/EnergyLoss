[Material]
friendly_name                   = Calcium carbonate
short_name                      = Calcium carbonate
specific_gravity                = 2.8 gram / centimeter ** 3
mean_excitation_energy          = 136.4 electron_volt
minimum_ionization              = 1.686 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 66.1 gram / centimeter ** 2
nuclear_interaction_length      = 99.4 gram / centimeter ** 2
pion_collision_length           = 93.0 gram / centimeter ** 2
pion_interaction_length         = 130.8 gram / centimeter ** 2
radiation_length                = 24.03 gram / centimeter ** 2
critical_energy_electrons       = 44.2 megaelectron_volt
critical_energy_positrons       = 42.94 megaelectron_volt
moliere_radius                  = 11.53 gram / centimeter ** 2
plasma_energy                   = 34.08 electron_volt
muon_critical_energy            = 632.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.49955

[Composition_0]
element                         = C
atomic_number                   = 6.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.120003

[Composition_1]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 3.0
mass_fraction                   = 0.479554

[Composition_2]
element                         = Ca
atomic_number                   = 20.0
atomic_fraction                 = 1.0
mass_fraction                   = 0.400443
