[Material]
friendly_name                   = Bakelite
short_name                      = Bakelite
specific_gravity                = 1.25 gram / centimeter ** 3
mean_excitation_energy          = 72.4 electron_volt
minimum_ionization              = 1.889 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 58.3 gram / centimeter ** 2
nuclear_interaction_length      = 83.4 gram / centimeter ** 2
pion_collision_length           = 85.7 gram / centimeter ** 2
pion_interaction_length         = 115.3 gram / centimeter ** 2
radiation_length                = 41.74 gram / centimeter ** 2
critical_energy_electrons       = 86.37 megaelectron_volt
critical_energy_positrons       = 84.07 megaelectron_volt
moliere_radius                  = 10.25 gram / centimeter ** 2
plasma_energy                   = 23.41 electron_volt
muon_critical_energy            = 1110.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.52792

[Composition_0]
element                         = H
atomic_number                   = 1.0
atomic_fraction                 = 38.0
mass_fraction                   = 0.057441

[Composition_1]
element                         = C
atomic_number                   = 6.0
atomic_fraction                 = 43.0
mass_fraction                   = 0.774591

[Composition_2]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 7.0
mass_fraction                   = 0.167968
