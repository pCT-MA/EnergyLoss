[Material]
friendly_name                   = Lutetium aluminum oxide 2
short_name                      = Lutetium aluminum oxide 2
specific_gravity                = 6.73 gram / centimeter ** 3
mean_excitation_energy          = 365.9 electron_volt
minimum_ionization              = 1.366 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 86.2 gram / centimeter ** 2
nuclear_interaction_length      = 138.1 gram / centimeter ** 2
pion_collision_length           = 112.9 gram / centimeter ** 2
pion_interaction_length         = 170.5 gram / centimeter ** 2
radiation_length                = 9.79 gram / centimeter ** 2
critical_energy_electrons       = 14.21 megaelectron_volt
critical_energy_positrons       = 13.75 megaelectron_volt
moliere_radius                  = 14.61 gram / centimeter ** 2
plasma_energy                   = 49.53 electron_volt
muon_critical_energy            = 234.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.43907

[Composition_0]
element                         = Lu
atomic_number                   = 71.0
atomic_fraction                 = 3.0
mass_fraction                   = 0.616224

[Composition_1]
element                         = Al
atomic_number                   = 13.0
atomic_fraction                 = 5.0
mass_fraction                   = 0.158379

[Composition_2]
element                         = O
atomic_number                   = 8.0
atomic_fraction                 = 12.0
mass_fraction                   = 0.225396
