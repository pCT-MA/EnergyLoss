[Material]
friendly_name                   = Viton fluoroelastomer
short_name                      = Viton fluoroelastomer
specific_gravity                = 1.8 gram / centimeter ** 3
mean_excitation_energy          = 98.6 electron_volt
minimum_ionization              = 1.701 centimeter ** 2 * megaelectron_volt / gram
nuclear_collision_length        = 62.9 gram / centimeter ** 2
nuclear_interaction_length      = 93.1 gram / centimeter ** 2
pion_collision_length           = 89.4 gram / centimeter ** 2
pion_interaction_length         = 123.8 gram / centimeter ** 2
radiation_length                = 35.36 gram / centimeter ** 2
critical_energy_electrons       = 65.82 megaelectron_volt
critical_energy_positrons       = 64.02 megaelectron_volt
moliere_radius                  = 11.39 gram / centimeter ** 2
plasma_energy                   = 26.95 electron_volt
muon_critical_energy            = 879.0 gigaelectron_volt
atomic_number_mass_number_ratio = 0.48585

[Composition_0]
element                         = H
atomic_number                   = 1.0
atomic_fraction                 = 2.0
mass_fraction                   = 0.009417

[Composition_1]
element                         = C
atomic_number                   = 6.0
atomic_fraction                 = 5.0
mass_fraction                   = 0.280555

[Composition_2]
element                         = F
atomic_number                   = 9.0
atomic_fraction                 = 8.0
mass_fraction                   = 0.710028
