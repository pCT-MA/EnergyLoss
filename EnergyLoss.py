#!/usr/bin/python3

# Contains the EnergyLoss and several related functions

# In order to use them, you must first define a UnitRegistry from the pint
# module and pass it to the functions. This forces users to use one system of
# units only. The `LoadMaterial` call assumes that you already scraped the data,
# using `SCrapePDG.py`:

'''
from AtomicNuclearProperties import LoadMaterial
from pint import UnitRegistry
ureg = UnitRegistry()

water = LoadMaterial("data/Water liquid.txt", ureg)
Thickness, Energy = EnergyLoss(200 * MeV, 200 * mm, water.ElectronDensity(), 1,
                               proton_mass, water.meanExcitationEnergy, ureg)
'''

from math import log, sqrt
from numpy import asarray


# Untested
# Returns the [Lorentz](https://en.wikipedia.org/wiki/Lorentz_factor) as a
# function of velocity
def GammaSquared(velocity, ureg):
    return 1 / (1 - velocity**2 / ureg.speed_of_light**2)


# Untested
# Returns the squared ratio of velocity and the _speed of light_ constant as a
# function of kinetic energy and mass
def BetaSquared(kineticEnergy, mass, ureg):
    restEnergy = 1 * mass * ureg.speed_of_light**2
    return 1 - (restEnergy / (kineticEnergy + restEnergy))**2


# Untested
def DensityCorrection(kineticEnergy, mass, ureg):
    return 0


# Untested
def StoppingPower(targetElectronDensity, kineticEnergy, projectileChargeNumber,
                  projectileMass, ionisationPotential, ureg):
    if kineticEnergy > 0:
        betaSquared = BetaSquared(kineticEnergy, projectileMass, ureg)

        return 4 * ureg.pi * targetElectronDensity * projectileChargeNumber**2 / \
            (1 * ureg.electron_mass * ureg.speed_of_light**2 * betaSquared) * \
            (ureg.elementary_charge**2 / (4 * ureg.pi * ureg.vacuum_permittivity))**2 * \
            (log(2 * ureg.electron_mass * ureg.speed_of_light**2 * betaSquared /
                 (ionisationPotential * (1 - betaSquared))) - betaSquared -
             DensityCorrection(kineticEnergy, projectileMass, ureg))
    return 0 * ureg.MeV / ureg.m


# Untested
def EnergyLoss(initialEnergy, thickness, targetElectronDensity,
               projectileChargeNumber, projectileMass, ionisationPotential,
               ureg, maxPrecision=10000):
    dt = thickness / maxPrecision
    energy = initialEnergy
    T = [0]
    E = [initialEnergy.to(ureg.MeV).magnitude]
    for t in range(maxPrecision):
        stoppingPower = StoppingPower(targetElectronDensity, energy,
                                      projectileChargeNumber, projectileMass,
                                      ionisationPotential, ureg)
        energy = max(0 * ureg.MeV, energy - stoppingPower * dt)
        E.append(energy.to(ureg.MeV).magnitude)
        T.append((dt * (t + 1)).to(ureg.mm).magnitude)
        if energy <= 0:
            break
    return (asarray(T) * ureg.mm, asarray(E) * ureg.MeV)


# Untested. Range values are larger than those found in
# [pstar tables](https://physics.nist.gov/PhysRefData/Star/Text/PSTAR.html)
#
# An issue was opened
# [here](http://git01.hephy.internal/proton-ct/EnergyLoss/-/issues/1)
def Range(initialEnergy, targetElectronDensity, projectileChargeNumber,
          projectileMass, ionisationPotential, ureg, stepSizeMM):
    energy = initialEnergy
    length = 0
    dt = stepSizeMM * ureg.mm
    while energy > 0:
        stoppingPower = StoppingPower(targetElectronDensity, energy,
                                      projectileChargeNumber, projectileMass,
                                      ionisationPotential, ureg)
        energy = max(0 * ureg.MeV, energy - stoppingPower * dt)
        length += dt
    return length


# Untested, Currently unused
# Returns the maximum energy transfer in a collision as a function of kinetic
# energy and mass of a particle
def MaximumEnergyTransfer(kineticEnergy, mass, ureg):
    velocity = ureg.speed_of_light * \
        sqrt(BetaSquared(kineticEnergy, mass, ureg))
    gammaSquared = GammaSquared(velocity, ureg)
    massRatio = 1 * ureg.electron_mass / mass
    return (2 * ureg.electron_mass * velocity**2 * gammaSquared) / \
        (1 + 2 * sqrt(gammaSquared) * massRatio + (massRatio)**2)


# Untested
# Returns the momentum times the velocity as a function of the kinetic energy
# and mass of a particle
def MomentumVelocity(kineticEnergy, mass, ureg):
    return (kineticEnergy * (kineticEnergy + 2 * mass * ureg.speed_of_light**2) /
            (kineticEnergy + 1 * mass * ureg.speed_of_light**2))
#                            ↑ converts unit to quantity
