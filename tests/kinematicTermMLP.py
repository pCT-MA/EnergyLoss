#! /usr/bin/python3

# %%
import context
import warnings
from pint import UnitRegistry, Quantity
from numpy.polynomial import polynomial
from numpy import asarray
from matplotlib import pyplot
from AtomicNuclearProperties import LoadMaterial
from EnergyLoss import EnergyLoss, MomentumVelocity


# shut up a pint message
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    Quantity([])

# %%
ureg = UnitRegistry()
ureg.setup_matplotlib(True)
proton_mass = ureg.proton_mass
MeV = ureg.MeV
mm = ureg.mm

h20 = LoadMaterial("../data/Water liquid.txt", ureg)
Thickness, Energy = EnergyLoss(200 * MeV, 200 * mm, h20.ElectronDensity(), 1,
                               proton_mass, h20.mean_excitation_energy, ureg)

KinematicTerm = asarray(
    [(1 / MomentumVelocity(e, proton_mass, ureg)**2).to(1/MeV**2).magnitude
     for e in Energy]) * (1/MeV**2)
fit = polynomial.polyfit(Thickness, KinematicTerm, 5)

# %%
pyplot.plot(Thickness, KinematicTerm)
pyplot.plot(Thickness,
            polynomial.polyval([t.to(mm).magnitude for t in Thickness], fit))
pyplot.ticklabel_format(style="sci", axis="y", scilimits=(0, 0))
pyplot.show()

# %%
