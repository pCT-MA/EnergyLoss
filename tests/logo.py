#! /usr/bin/python3
# Script used to produce the logo of this repository

# %%
import context
from AtomicNuclearProperties import LoadMaterial
from EnergyLoss import EnergyLoss

from bisect import bisect
from matplotlib import pyplot
from pint import UnitRegistry, Quantity
import warnings

# shut up a pint message
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    Quantity([])

# %%
ureg = UnitRegistry()
ureg.setup_matplotlib(True)
Al = LoadMaterial("../data/Al.txt", ureg)
Thickness, Energy = EnergyLoss(100 * ureg.MeV, 200 * ureg.mm,
                               Al.ElectronDensity(), 1, ureg.proton_mass,
                               Al.mean_excitation_energy, ureg)
# %%
pyplot.figure(figsize=(4, 3), constrained_layout=True)
pyplot.title("Traced energy loss in Aluminium")
pyplot.plot(Thickness, Energy, linewidth=5)
pyplot.annotate(
    "Traced thickness: {:.2f~P}\nTraced residual energy: {:.2f~P}".format(
        Thickness[-1], Energy[-1]),
    (0.9, 0.15), xycoords='axes fraction',
    horizontalalignment="right", verticalalignment="top")

thickness = 10 * ureg.mm
energy = Energy[bisect(Thickness, thickness) - 1]
pyplot.annotate("P: ({:.2f~P}, {:.2f~P})".format(thickness, energy),
                (thickness, energy), xycoords='data',
                xytext=(5, 5), textcoords="offset points",
                horizontalalignment="left", verticalalignment="bottom")
pyplot.axhline(energy, color="black", linestyle=":")
pyplot.axvline(thickness, color="black", linestyle=":")
pyplot.savefig("TracedEnergyLoss.png")
pyplot.show()


# %%
