#! /usr/bin/python3

# This is not actual testing code but serves to visually compare Nist and
# _this-library calculated_ energy loss

# %%
from matplotlib import pyplot
from pint import UnitRegistry
from numpy import asarray

from LoadNist import LoadNist
import context
from AtomicNuclearProperties import LoadMaterial
from EnergyLoss import StoppingPower

# %%
ureg = UnitRegistry()
ureg.setup_matplotlib(True)
# %%


def StoppingPowers(energies, material, charge, mass):
    return asarray(
        [(StoppingPower(material.ElectronDensity(), e, charge, mass,
                        material.mean_excitation_energy, ureg) /
          material.specific_gravity).to(ureg.MeV * ureg.cm**2 / ureg.g).magnitude
         for e in energies]) * ureg.MeV * ureg.cm**2 / ureg.g


material = LoadMaterial("../data/Water liquid.txt", ureg)

proton_nist_energy, proton_nist_stopping_power = LoadNist("WATER_LIQUID.txt",
                                                          ureg)
helium_nist_energy, helium_nist_stopping_power = LoadNist("HE_WATER_LIQUID.txt",
                                                          ureg)

proton_stopping_power = StoppingPowers(proton_nist_energy, material, 1,
                                       ureg.proton_mass)
helium_stopping_power = StoppingPowers(helium_nist_energy, material, 2,
                                       4.002602 * ureg.dalton)

pyplot.figure(constrained_layout=True)
pyplot.xscale("log")
pyplot.yscale("log")

pyplot.plot(proton_nist_energy, proton_nist_stopping_power, ".",
            label="NIST proton", color="tab:blue")
pyplot.plot(helium_nist_energy, helium_nist_stopping_power, ".",
            label="NIST helium", color="tab:orange")

pyplot.plot(proton_nist_energy, proton_stopping_power,
            label="proton", color="tab:blue")
pyplot.plot(helium_nist_energy, helium_stopping_power,
            label="helium", color="tab:orange")

pyplot.legend()
pyplot.show()

# %%
