#! /usr/bin/python3
import csv
from numpy import asarray


def LoadNist(nist_file, unit_registry, first_index=0, last_index=None):
    nist_energy = []
    nist_stopping_power = []
    with open(nist_file) as csv_file:
        for i in range(8):
            csv_file.readline()
        csv_reader = csv.reader(csv_file, delimiter=' ')
        for row in csv_reader:
            nist_energy.append(float(row[0]))
            nist_stopping_power.append(float(row[3]))

    return (asarray(nist_energy[first_index:last_index]) * unit_registry.MeV,
            asarray(nist_stopping_power[first_index:last_index])
            * unit_registry.MeV * unit_registry.cm**2 / unit_registry.g)
