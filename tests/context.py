# Gives the tests import context, e.g. properly finds the energy-loss library
import os
import sys
sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')))
