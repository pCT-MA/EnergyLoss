#!/usr/bin/python3

import unittest
from numpy import asarray
from pint import UnitRegistry

from LoadNist import LoadNist

import context
from AtomicNuclearProperties import LoadMaterial
from EnergyLoss import StoppingPower

ureg = UnitRegistry()
material_file = "../data/Water liquid.txt"
nist_file = "WATER_LIQUID.txt"
index_interval = (90, 116)  # [60 MeV, 800 MeV]


class CompareNistTest(unittest.TestCase):
    def test(self):
        material = LoadMaterial(material_file, ureg)
        nist_energy, nist_stopping_power = LoadNist(
            nist_file, ureg, *index_interval)

        stopping_power = asarray(
            [(StoppingPower(material.ElectronDensity(), e, 1, ureg.proton_mass,
                            material.mean_excitation_energy, ureg) /
              material.specific_gravity).to(ureg.MeV * ureg.cm**2 / ureg.g).magnitude
             for e in nist_energy]) * ureg.MeV * ureg.cm**2 / ureg.g

        for s in range(len(nist_stopping_power)):
            relative_error = abs(
                1 - stopping_power[s] / nist_stopping_power[s])
            self.assertLessEqual(relative_error, 0.01)
