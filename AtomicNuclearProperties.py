#!/usr/bin/python3

# Defines Material classes using properties from
# [here](http://pdg.lbl.gov/2019/AtomicNuclearProperties/)

# In order to create an instance, you must first define a UnitRegistry from the
# pint module and give it to the constructor of the material. This forces users
# to use one system of units only:

# Note, that this snippet assumes that you previously scraped the PDG data with
# `ScrapePDG.py`

'''
from AtomicNuclearProperties import LoadMaterial
from pint import UnitRegistry
ureg = UnitRegistry()
Al = LoadMaterial("data/Al.txt")
'''

# Some materials, such as elements, directly inherit from the Material class.
# Crystals or mixtures on the other hand inherit from the CompositeMaterial,
# which additionally stores the composition (CompositionPart class).

from configparser import ConfigParser


class Material:
    friendly_name = None
    short_name = None
    atomic_number = None
    atomic_mass = None
    specific_gravity = None
    mean_excitation_energy = None
    minimum_ionization = None
    nuclear_collision_length = None
    nuclear_interaction_length = None
    pion_collision_length = None
    pion_interaction_length = None
    radiation_length = None
    critical_energy_electrons = None
    critical_energy_positrons = None
    moliere_radius = None
    plasma_energy = None
    muon_critical_energy = None
    melting_point = None
    boiling_point = None
    index_of_refraction = None
    unit_registry = None

    def __init__(self,
                 friendly_name=None,
                 short_name=None,
                 atomic_number=None,
                 atomic_mass=None,
                 specific_gravity=None,
                 mean_excitation_energy=None,
                 minimum_ionization=None,
                 nuclear_collision_length=None,
                 nuclear_interaction_length=None,
                 pion_collision_length=None,
                 pion_interaction_length=None,
                 radiation_length=None,
                 critical_energy_electrons=None,
                 critical_energy_positrons=None,
                 moliere_radius=None,
                 plasma_energy=None,
                 muon_critical_energy=None,
                 melting_point=None,
                 boiling_point=None,
                 index_of_refraction=None,
                 unit_registry=None):
        self.friendly_name = friendly_name
        self.short_name = short_name
        self.atomic_number = atomic_number
        self.atomic_mass = atomic_mass
        self.specific_gravity = specific_gravity
        self.mean_excitation_energy = mean_excitation_energy
        self.minimum_ionization = minimum_ionization
        self.nuclear_collision_length = nuclear_collision_length
        self.nuclear_interaction_length = nuclear_interaction_length
        self.pion_collision_length = pion_collision_length
        self.pion_interaction_length = pion_interaction_length
        self.radiation_length = radiation_length
        self.critical_energy_electrons = critical_energy_electrons
        self.critical_energy_positrons = critical_energy_positrons
        self.moliere_radius = moliere_radius
        self.plasma_energy = plasma_energy
        self.muon_critical_energy = muon_critical_energy
        self.melting_point = melting_point
        self.boiling_point = boiling_point
        self.index_of_refraction = index_of_refraction
        self.unit_registry = unit_registry

    def MinimumIonization(self):
        return self.minimum_ionization * self.specific_gravity

    def NuclearCollisionLength(self):
        return self.nuclear_collision_length / self.specific_gravity

    def NuclearInteractionLength(self):
        return self.nuclear_interaction_length / self.specific_gravity

    def PionCollisionLength(self):
        return self.pion_collision_length / self.specific_gravity

    def PionInteractionLength(self):
        return self.pion_interaction_length / self.specific_gravity

    def RadiationLength(self):
        return self.radiation_length / self.specific_gravity

    def MoliereRadius(self):
        return self.moliere_radius / self.specific_gravity

    def ElectronDensity(self):
        return (self.atomic_number / self.atomic_mass * self.specific_gravity /
                self.unit_registry.dalton * self.unit_registry.g /
                self.unit_registry.mole)

    def __str__(self):
        return "\n".join(list(filter(None, [
            "[Material]",
            _format_string("friendly_name                   = %s",
                           self.friendly_name),
            _format_string(
                "short_name                      = %s", self.short_name),
            _format_string("atomic_number                   = %s",
                           self.atomic_number),
            _format_string(
                "atomic_mass                     = %s", self.atomic_mass),
            _format_string("specific_gravity                = %s",
                           self.specific_gravity),
            _format_string("mean_excitation_energy          = %s",
                           self.mean_excitation_energy),
            _format_string("minimum_ionization              = %s",
                           self.minimum_ionization),
            _format_string("nuclear_collision_length        = %s",
                           self.nuclear_collision_length),
            _format_string("nuclear_interaction_length      = %s",
                           self.nuclear_interaction_length),
            _format_string("pion_collision_length           = %s",
                           self.pion_collision_length),
            _format_string("pion_interaction_length         = %s",
                           self.pion_interaction_length),
            _format_string("radiation_length                = %s",
                           self.radiation_length),
            _format_string("critical_energy_electrons       = %s",
                           self.critical_energy_electrons),
            _format_string("critical_energy_positrons       = %s",
                           self.critical_energy_positrons),
            _format_string("moliere_radius                  = %s",
                           self.moliere_radius),
            _format_string("plasma_energy                   = %s",
                           self.plasma_energy),
            _format_string("muon_critical_energy            = %s",
                           self.muon_critical_energy),
            _format_string("melting_point                   = %s",
                           self.melting_point),
            _format_string("boiling_point                   = %s",
                           self.boiling_point),
            _format_string("index_of_refraction             = %s",
                           self.index_of_refraction)])) + [""])

    def SaveTo(self, filename):
        with open(filename, "w") as outFile:
            outFile.write(str(self))


class CompositionPart:
    element = None
    atomic_number = None
    atomic_fraction = None
    mass_fraction = None

    def __init__(self,
                 element,
                 atomic_number,
                 atomic_fraction,
                 mass_fraction):
        self.element = element
        self.atomic_number = atomic_number
        self.atomic_fraction = atomic_fraction
        self.mass_fraction = mass_fraction


class CompositeMaterial(Material):
    composition = []
    atomic_number_mass_number_ratio = None

    def __init__(self, atomic_number_mass_number_ratio=None, composition=[]):
        self.atomic_number_mass_number_ratio = atomic_number_mass_number_ratio
        self.composition = composition

    def __str__(self):
        base_string = Material.__str__(self)
        base_string += "\n".join(list(filter(None, [
            _format_string(
                "atomic_number_mass_number_ratio = %s",
                self.atomic_number_mass_number_ratio)])) + [""])
        p = 0
        for part in self.composition:
            base_string += "\n".join(
                [""] +
                list(filter(None, [
                    "[Composition_%d]" % p,
                    _format_string(
                        "element                         = %s", part.element),
                    _format_string(
                        "atomic_number                   = %s", part.atomic_number),
                    _format_string(
                        "atomic_fraction                 = %s", part.atomic_fraction),
                    _format_string(
                        "mass_fraction                   = %s", part.mass_fraction),
                ]))
                + [""])
            p += 1
        return base_string

    def ElectronDensity(self):
        return (self.atomic_number_mass_number_ratio * self.specific_gravity /
                self.unit_registry.dalton)


def _format_string(key, value):
    return key % str(value) if value != None else None


def _parse_unit(key, value, unit_registry):
    if key in ["friendly_name", "short_name"]:
        return value
    return unit_registry.parse_expression(value)


def LoadMaterial(filename, unit_registry):
    config = ConfigParser()
    config.read(filename)
    m = Material() if len(config.sections()) < 2 else CompositeMaterial()
    m.unit_registry = unit_registry
    compositions = []
    for section in config.sections():
        if section == "Material":
            for key in config[section]:
                setattr(m, key, _parse_unit(
                    key, config[section][key], unit_registry))
        elif "Composition" in section:
            composition = config[section]
            compositions.append(CompositionPart(
                composition["element"], composition["atomic_number"],
                composition["atomic_fraction"], composition["mass_fraction"]))
    if len(compositions) > 0 and type(m) == CompositeMaterial:
        m.composition = compositions
    return m
