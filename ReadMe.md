# Model energy of a charged particle as a function of depth in a uniform material

The energy loss at a given energy is evaluated using the
[Bethe formula](https://en.wikipedia.org/wiki/Bethe_formula):

```math
S(E) = -\left<\frac{dE}{dx}\right> =
\frac{4\pi}{m_e c^2} \cdot \frac{nz^2}{\beta^2} \cdot 
\left(\frac{e^2}{4\pi \varepsilon_0}\right)^2 \cdot
\left[\ln\left(\frac{2 m_e c^2 \beta^2}{I (1 - \beta^2)}\right) - \beta^2\right]
```

where $`c`$ is the speed of light, $`\varepsilon_0`$ the vacuum permittivity, 
$`\beta = \frac{v}{c}`$ the ratio of the projectile velocity and the speed of
light, $`e`$ and $`m_e`$ the elementary charge and the electron rest mass and
$`n`$ the electron density, which is calculated as

```math
n = \frac{N_A Z \rho}{A M_u}
```

where $`\rho`$, $`Z`$ and $`A`$ are the material density, atomic number and
relative atomic mass, $`N_A`$ is the Avogadro constant and $`M_u`$ the molar
mass constant. Note that the given energy loss implies an approximation of the
_maximum energy transfer in a single collision_ $`W_\mathrm{max}`$ to low
energies. It is implicitly used as

```math
W_\mathrm{max} = 2 m_\mathrm{e} c^2 \beta^2 \gamma^2,
```

and is only valid for $`2\gamma m_\mathrm{e} \ll M`$, where $`M`$ is the
projectile mass. Please refer to the
[Review of Particle Physics](https://doi.org/10.1103/PhysRevD.98.030001) for
more general models of energy loss.

The velocity $`\beta`$ is converted by using the kinetic energy
$`E_\mathrm{kin}`$ and rest mass $`m`$ of a particle

```math
\beta^2 = 1 - \left(\frac{mc^2}{E_\mathrm{kin} + m c^2}\right)^2
```

## Transporting energy loss

The energy after a thickness $`x`$ of a material is numerically evaluated by
splitting up the total amount of material into $`n`$ (precision) equal slices
$`dx`$, then calculating the local energy at each slice using the Bethe formula.
Using an initial energy $`E_0`$

```math
E_1 = E(dx) = E_0 - S(E_0) \times dx
```

```math
E_2 = E(2 dx) = E_1 - S(E_1) \times dx
```

Generally

```math
E_i = E_{i - 1} - S(E_{i - 1}) \times dx \quad \textrm{for} \quad  i \in [1, n]
```

## Using this repository

I really like the way git repositories and submodules work, which is why I use
them as much as possible. This project should be used like a library, so it is a
good candidate for a submodule. Otherwise, you can simply clone it. For both of
these methods, I am assuming a directory structure where the _EnergyLoss_
repository is at the same level as your python scripts:

```disable-highlight
your-project/EnergyLoss
your-project/your-scripts.py
```

Enter your own project directory and use one of the methods below: 

```bash
cd your-project
git clone git@gitlab.com:pCT-MA/EnergyLoss          # preferred method
git clone https://gitlab.com/pCT-MA/EnergyLoss.git  # if you didnt configure SSH in your account
git submodule add git@gitlab.com:pCT-MA/EnergyLoss  # to add as submodule
git submodule add https://gitlab.com/pCT-MA/EnergyLoss.git
```

Then, you'll be able to transport the energy loss analytically, using e.g.: 

```python
from pint import UnitRegistry
from matplotlib import pyplot
from EnergyLoss.AtomicNuclearProperties import LoadMaterial
from EnergyLoss.EnergyLoss import EnergyLoss

ureg = UnitRegistry()
ureg.setup_matplotlib(True)
Al = LoadMaterial("EnergyLoss/data/Al.txt", ureg)
Thickness, Energy = EnergyLoss(100 * ureg.MeV,              # primary energy
                               200 * ureg.mm,               # maximum target depth
                               Al.ElectronDensity(),        # target electron density
                               1,                           # projectile charge
                               ureg.proton_mass,            # projectile mass
                               Al.mean_excitation_energy,   # target ionization potential
                               ureg)                        # system of units

pyplot.plot(Thickness, Energy)
pyplot.show()
```

![example.png](docs/example.png)

There are some other examples in the [tests](tests) directory, such as
[comparisons to the PSTAR NIST data](tests/nistPlot.py).

## Roadmap

### Nice to have

One usability improvement would be to take the material classes as function
parameters. With a new particle class, this would allow us to hide the
mathematical details within the stopping power or energy loss functions: 

```python
my_particle = Particle(…)
my_target = Material(…)
EnergyLoss(my_particle, my_target, unit_registry)
```

We could even take this further in terms of proper object oriented design, using
strategy objects for the energy loss-, maximum energy transfer- and correction
term-models: 

```mermaid
classDiagram

class EnergyLossModel{
    <<interface>>
    +__call__(particle, material, unit_registry)* [thickness], [energy]
}
Particle -- EnergyLossModel
Material -- EnergyLossModel
UnitRegistry -- EnergyLossModel

class BetheBlockEnergyLossModel {
    +__call__(particle, material, unit_registry) [thickness], [energy]
}
EnergyLossModel <-- BetheBlockEnergyLossModel

class MaximumEnergyTransferModel { <<interface>> }
MaximumEnergyTransferModel -- BetheBlockEnergyLossModel

class CorrectionModel { <<interface>> }
CorrectionModel -- BetheBlockEnergyLossModel
```

In the same way, we could use strategies for the energy transport method itself.

## See also

- Energy deposition in a uniform medium, using
[offline NIST data](https://gitlab.com/pCT-MA/edepfromnist): Stopping power
and energy loss graphs are available for a wide variety of materials. This
project supports protons and electrons, and may potentially use alpha
particles in future updates. 