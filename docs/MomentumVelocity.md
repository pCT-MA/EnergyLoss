# Momentum-velocity $`pv(E_\text{kin})`$

Using

```math
E_\text{total} = \gamma m c^2
```
```math
E_\text{kin} = (\gamma - 1)m c^2
```
```math
p = \gamma m v
```
```math
E_\text{total}^2 = (pc)^2 + (mc^2)^2
```

we obtain

```math
(E_\text{kin} + mc^2)^2 = (pc)^2 + (mc^2)^2
```
```math
E_\text{kin}^2 + 2 E_\text{kin} mc^2 + \cancel{m^2c^4} =
p^2c^2 + \cancel{m^2c^4}
```
```math
p^2 = p \cdot \gamma m v \rightarrow
E_\text{kin}^2 + 2 E_\text{kin} mc^2 = p\gamma m v c^2
```
```math
pv = \frac{E_\text{kin}(E_\text{kin} + 2 mc^2)}{\gamma m c^2} =
\frac{E_\text{kin}(E_\text{kin} + 2 mc^2)}{E_\text{total}} =
\frac{E_\text{kin}(E_\text{kin} + 2 mc^2)}{E_\text{kin} + mc^2}
```
