#! /usr/bin/python3
# Source: [Donahue 2016](http://dx.doi.org/10.1088/0031-9155/61/17/6570)
# %%
from scipy.optimize import fsolve
from AtomicNuclearProperties import LoadMaterial
from pint import UnitRegistry
from numpy import exp


def Kappa(projectileCharge, targetElectronDensity, ureg):
    return (2 * ureg.pi * ureg.classical_electron_radius**2 *
            targetElectronDensity * ureg.electron_mass * ureg.speed_of_light**2
            * projectileCharge**2)


def RangeMM(projectileCharge, targetElectronDensity, beta, q, alpha, p, h, g,
            energyMeVPerNucleon, projectileAtomicMassNumber, ureg):
    return (projectileAtomicMassNumber / Kappa(projectileCharge,
                                               targetElectronDensity, ureg) *
            (beta * energyMeVPerNucleon**q + alpha * energyMeVPerNucleon**p +
             h / g *
             (exp(-g * energyMeVPerNucleon) + g * energyMeVPerNucleon - 1))
            * ureg.MeV).to(ureg.mm).magnitude


# Returns the CSDA range [mm] of protons with the provided energy in water
def RangeCsda(energyMeV):
    ureg = UnitRegistry()
    h2o = LoadMaterial("data/Water liquid.txt", ureg)
    return RangeMM(1, h2o.ElectronDensity(), 3.1764e-5, 0.4583, 1.3127e-4,
                   1.6493, 2.1239e-2, 4.1618e-3, energyMeV, 1, ureg)


# Returns the initial energy [MeV] of protons with the provided range in water
def EnergyCsda(rangeCsdaMm, initialGuess=100):
    ureg = UnitRegistry()
    h2o = LoadMaterial("data/Water liquid.txt", ureg)
    return fsolve(lambda E: rangeCsdaMm - RangeMM(1, h2o.ElectronDensity(),
                                                  3.1764e-5, 0.4583, 1.3127e-4,
                                                  1.6493, 2.1239e-2, 4.1618e-3,
                                                  E, 1, ureg), initialGuess)


# Testing
if __name__ == "__main__":
    from matplotlib import pyplot

    energies = [250 + i * 50 for i in range(5)]
    pStarRangesMm = [379.4, 514.5, 662.8, 822.5, 991.2]

    pyplot.plot(energies, pStarRangesMm, ".-", color="black", label="P* NIST")
    pyplot.plot(energies, [RangeCsda(e) for e in energies],
                label="Donahue, 2016")
    pyplot.xlabel("Energy [MeV]")
    pyplot.ylabel("Range [mm]")
    pyplot.legend()

    twin = pyplot.gca().twinx()
    twin.plot(energies,
              [pStarRangesMm[e] - RangeCsda(energies[e])
               for e in range(len(energies))],
              ls="dotted")
    pyplot.ylabel("Range error [mm] (dotted)")
    pyplot.show()

# %%
