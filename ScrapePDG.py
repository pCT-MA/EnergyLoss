#!/usr/bin/python3

'''
# download the entire webpage into the default `pages` directory
ScrapePDG(2020)

# parse the pages and store them in the default `data` directory
ParsePDG("pages/elements.cfg", UnitRegistry(), ElementNamingStrategy())
ParsePDG("pages/physical_states.cfg", UnitRegistry())
ParsePDG("pages/composites.cfg", UnitRegistry())
'''

# %%
import re
from pint import DefinitionSyntaxError
import requests
from bs4 import BeautifulSoup
from AtomicNuclearProperties import Material, CompositeMaterial, CompositionPart
import warnings
from os import makedirs
from os.path import isdir
from pint import UnitRegistry
# %%


def StorePage(url, target):
    directory = "/".join(target.rsplit("/")[:-1])
    if not isdir(directory):
        makedirs(directory)

    r = requests.get(url)
    with open(target, "w") as outFile:
        outFile.writelines(r.text)


def AppendSlashDirectory(path):
    return path if path[-1] == "/" else path + "/"


def ScrapeElements(baseUrl, soup, outDirectory="pages"):
    targets = []
    for a in soup.find_all("a"):
        if a.has_attr("href"):
            if "HTML/" in a["href"]:
                targets.append(AppendSlashDirectory(outDirectory) + a["href"])
                StorePage(baseUrl + a["href"], targets[-1])
    return targets


def ScrapeComposites(baseUrl, soup, outDirectory="pages"):
    targets = []
    for option in soup.find_all("option"):
        if option.has_attr("value"):
            if option["value"] != "#":
                targets.append(AppendSlashDirectory(
                    outDirectory) + option["value"])
                StorePage(baseUrl + option["value"], targets[-1])
    return targets


def ScrapePhysicalStates(baseUrl, soup, outDirectory="pages"):
    targets = []
    for a in soup.find_all("a"):
        if a.has_attr("onmouseover"):
            links = a["onmouseover"].split("<br>")
            for l in links:
                sub = (l.split("href=")[1].split(">")[0])
                targets.append(
                    AppendSlashDirectory(outDirectory) + sub)
                StorePage(baseUrl + sub, targets[-1])
    return targets


def ScrapePDG(year, pagesDirectory="pages"):
    unit_registry = UnitRegistry()
    baseUrl = "https://pdg.lbl.gov/%s/AtomicNuclearProperties/" % year
    r = requests.get(baseUrl)
    soup = BeautifulSoup(r.text)
    targets = ScrapeElements(baseUrl, soup, pagesDirectory)
    with open("pages/elements.cfg", "w") as configFile:
        configFile.writelines("\n".join(targets))
    targets = ScrapeComposites(baseUrl, soup, pagesDirectory)
    with open("pages/composites.cfg", "w") as configFile:
        configFile.writelines("\n".join(targets))
    targets = ScrapePhysicalStates(baseUrl, soup, pagesDirectory)
    with open("pages/physical_states.cfg", "w") as configFile:
        configFile.writelines("\n".join(targets))


def IsComposite(parse_result):
    return len(parse_result) > 1


def PrepareValue(expression):
    if "[" in expression and "]" in expression:
        expression = expression.split("[")[1].split("]")[0]
    return expression.split("(")[0]


def PrepareUnit(expression):
    groups = []
    for g in expression.split(" "):
        split = re.findall(r'-?\d+|[a-zA-Z]+|\W+?', g)
        if len(split) > 1:
            last_group = 0
            for pointer in range(len(split)):
                string = split[pointer]
                if string.lstrip("-+").isnumeric():
                    groups.append("^".join(split[last_group:pointer+1]))
                    last_group = pointer + 1
        else:
            groups.append(g)
    return " ".join(groups)


def ParseExpression(ureg, value, unit):
    return ureg.parse_expression(
        PrepareValue(value) + " " + PrepareUnit(unit))


class ScrapedMaterial(Material):
    patterns_members = {"Atomic number": "atomic_number",
                        "Atomic mass": "atomic_mass",
                        "Specific gravity": "specific_gravity",
                        "Specific gravity \n\n(20° C, 1 atm)": "specific_gravity",
                        "Mean excitation energy": "mean_excitation_energy",
                        "Minimum ionization": "minimum_ionization",
                        "Nuclear collision length": "nuclear_collision_length",
                        "Nuclear interaction length": "nuclear_interaction_length",
                        "Pion collision length": "pion_collision_length",
                        "Pion interaction length": "pion_interaction_length",
                        "Radiation length": "radiation_length",
                        "Molière radius": "moliere_radius",
                        "Plasma energy ℏωp": "plasma_energy",
                        "Muon critical energy": "muon_critical_energy",
                        "Melting point": "melting_point",
                        "Boiling point @ 1 atm": "boiling_point",
                        "Index of refraction (Na D)": "index_of_refraction",
                        "Index of refraction \n\n(0° C, 1 atm, Na D)": "index_of_refraction"}
    # special case, since electron and positron patterns are on the same row
    critical_energy_pattern = "Critical energy"

    def __init__(self, tables, unit_registry):
        self.unit_registry = unit_registry
        if len(tables) > 0:
            for row in tables[0][1:]:
                if len(row) > 0:
                    missedKey = True
                    key = row[0]
                    for pattern in self.patterns_members:
                        if key == pattern:
                            try:
                                parsed = ParseExpression(self.unit_registry,
                                                         row[1], row[2])
                                setattr(self, self.patterns_members[pattern],
                                        parsed)
                            except DefinitionSyntaxError as identifier:
                                warnings.warn(
                                    "Cannot currently parse unit for '" +
                                    row[2] + "'",
                                    RuntimeWarning)
                            missedKey = False

                    if key == self.critical_energy_pattern and len(row) > 2:
                        setattr(self, "critical_energy_electrons",
                                ParseExpression(self.unit_registry,
                                                row[1], row[2]))
                        if len(row) > 4:
                            setattr(self, "critical_energy_positrons",
                                    ParseExpression(self.unit_registry,
                                                    row[3], row[4]))
                        missedKey = False

                    if missedKey:
                        warnings.warn("Missed key: '" + key + "'")


class ScrapedCompositeMaterial(ScrapedMaterial, CompositeMaterial):
    def __init__(self, tables, unit_registry):
        self.patterns_members["<Z/A>"] = "atomic_number_mass_number_ratio"
        ScrapedMaterial.__init__(self, tables, unit_registry)
        if len(tables) > 1:
            composition = []
            for row in tables[1][1:-1]:
                # standard rock special case
                if row[3] == "* Along with density above, definition of standard rock":
                    atomic_number = row[0].split(",")[0].split("Z = ")[1]
                    composition.append(CompositionPart(row[0].split("  ")[0], float(
                        atomic_number), float(row[1]), float(row[2])))
                else:
                    composition.append(CompositionPart(
                        row[0], float(row[1]), float(row[2]), float(row[3])))
            self.composition = composition


def ExtractTables(text):
    soup = BeautifulSoup(text)

    data = []
    for table in soup.find_all("table"):
        data.append([])
        for row in table.find_all("tr"):
            data[-1].append([element.text.strip()
                             for element in row.find_all("td")])
    return data


def ParseHtmlFile(filepath, unit_registry, name, short_name):
    material = None
    with open(filepath) as inFile:
        lines = inFile.readlines()
        tables = ExtractTables("".join(lines))
        material = (ScrapedCompositeMaterial(tables, unit_registry)
                    if IsComposite(tables)
                    else ScrapedMaterial(tables, unit_registry))
        material.friendly_name = name
        material.short_name = short_name
    return material


class NamingStrategy:
    def __call__(self, filename):
        raise NotImplementedError


class DefaultNamingStrategy(NamingStrategy):
    def __call__(self, filename):
        name_only = filename.split("/")[-1].split(".")[0].replace("_", " ")
        name = name_only[0].upper() + name_only[1:]
        return name, name


class ElementNamingStrategy(NamingStrategy):
    def __call__(self, filename):
        name_only = filename.split("/")[-1].split(".")[0].replace("_", " ")
        friendly_name = " ".join(name_only.split(" ")[:-1])
        element_name = name_only.split(" ")[-1]
        return (friendly_name[0].upper() + friendly_name[1:],
                element_name[0].upper() + element_name[1:])


def ParsePDG(configFile, unit_registry, naming_strategy=DefaultNamingStrategy(),
             dataDirectory="data"):
    if not isdir(dataDirectory):
        makedirs(dataDirectory)
    with open(configFile) as inFile:
        for line in inFile:
            m = ParseHtmlFile(line.split("\n")[0], unit_registry,
                              *naming_strategy(line))
            m.SaveTo(AppendSlashDirectory(
                dataDirectory) + m.short_name + ".txt")
