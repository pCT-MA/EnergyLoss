#!/usr/bin/python3
# %% Contains a refactored version of the code that Florian Pitters put up in
# [#proton-ct-meetings](https://hephy-dd.slack.com/archives/CFDR2TV6W/p1585127141008700)

# The functions RangeCsda, EnergyCsda and ResidualEnergyCsda originally came
# from [Ulmer, 2010](http://dx.doi.org/10.1140/epjst/e2010-01335-7)

from numpy import exp


# Returns the CSDA range [mm] of protons with the provided energy in water
def RangeCsda(energyMeV):
    a = 6.94656e-3
    b = [15.14450027, 29.84400076]
    g = [0.001260021, 0.003260031]
    return 10 * a * energyMeV * sum([1] + [b[i] * (1 - exp(-g[i] * energyMeV))
                                           for i in range(2)])


# Returns the initial energy [MeV] of protons with the provided range in water
def EnergyCsda(rangeCsdaMm):
    rangeCsdaCm = rangeCsdaMm / 10
    c = [96.63872, 25.0472, 8.80745, 4.19001, 9.2732]
    l = [0.0975, 1.24999, 5.7001, 10.6501, 106.72784]
    return rangeCsdaCm * sum([c[i] * exp(-rangeCsdaCm / l[i])
                              for i in range(5)])


# Testing
if __name__ == "__main__":
    from matplotlib import pyplot
    from numpy import linspace
    # reproduce Figure 2 of the paper linked above
    energies = linspace(0, 270)
    ranges = [RangeCsda(energy) for energy in energies]
    pyplot.plot(energies, ranges)
    pyplot.xlim(0, 270)
    pyplot.ylim(0, 440)
    pyplot.xlabel("Energy [MeV]")
    pyplot.ylabel(r"$\mathrm{R}_\mathrm{CSDA}$ [mm]")
    pyplot.show()
    pyplot.close()
    # reproduce Figure 4 of the paper linked above
    inverseEnergies = [EnergyCsda(RangeCsda(energy)) for energy in energies]
    pyplot.plot(ranges, inverseEnergies)
    pyplot.xlim(0, 440)
    pyplot.ylim(0, 270)
    pyplot.xlabel(r"$\mathrm{R}_\mathrm{CSDA}$ [mm]")
    pyplot.ylabel(r"$\mathrm{E}_0$ [MeV]")
    pyplot.show()
    pyplot.close()

    testValues = [259.6, 379.4, 514.5, 662.8, 822.5]
    expected = [200, 250, 300, 350, 400]
    errors = [expected[t] - EnergyCsda(testValues[t])
              for t in range(len(testValues))]

    pyplot.plot(testValues, expected, label="P* NIST")
    pyplot.plot(testValues, [EnergyCsda(t) for t in testValues],
                ls="dashed", label="EnergyCsda(range)")
    pyplot.axhline(300, color="black", ls="dotted", alpha=0.5)
    pyplot.axvline(500, color="black", ls="dotted", alpha=0.5)
    pyplot.xlabel("Range [cm]")
    pyplot.ylabel("Energy [MeV]")
    pyplot.legend()

    pyplot.gca().twinx()
    pyplot.plot(testValues, errors, label=r"$\Delta E$", color="tab:green")
    pyplot.ylabel(r"$\Delta E$ [MeV]", color="tab:green")
    pyplot.title("CSDA energy using Ulmer, 2010")
    pyplot.show()


# %%

# This is left to do (changing from water to another medium):
# R CSDA (medium) = R CSDA (water) · (Z · ρ/A N ) water · (A N /Z · ρ) medium
